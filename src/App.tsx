import axios, { AxiosResponse } from 'axios';
import * as React from 'react';

import { Paper } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import LabelIcon from '@material-ui/icons/Label';
import RemoveIcon from '@material-ui/icons/Remove';

import { ICategory } from './model/model';

const styles = (theme: Theme) => createStyles({
  button: {
    margin: theme.spacing.unit,
  },
  category: {
    flex: "1 1",
  },
  counter: {
    flex: "auto",
  },
  root: theme.mixins.gutters({
    margin: "auto",
    maxWidth: 900,
    padding: 16,
  }),
});

interface IProps extends WithStyles<typeof styles> { }

interface IStates {
  categories: ICategory[]
}

class App extends React.Component<IProps, IStates> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      categories: []
    }
  }

  public componentDidMount() {
    axios.get("/api/v1/category")
      .then((response: AxiosResponse<ICategory[]>) => {
        const categoriesData: ICategory[] = response.data;
        this.setState({
          categories: categoriesData
        })
      })
  }

  public render() {
    const classes = this.props.classes;
    return (
      <div>
        <Paper className={classes.root}>
          <Typography variant="headline" component="h3" color="primary" align="center">
            Benchmark Record
          </Typography>
          <List component="nav">
            {this.state.categories.map((category) =>
              <ListItem divider={true} button={true} key={category.id}>
                <ListItemIcon>
                  <LabelIcon />
                </ListItemIcon>
                <ListItemText className={classes.category} primary={category.name} />
                <Typography variant="headline" component="h3" color="secondary" align="center" className={classes.counter}>
                  {category.count}
                </Typography>
                <Button
                  variant="fab"
                  color="primary"
                  aria-label="add"
                  className={this.props.classes.button}
                  mini={true}
                  // tslint:disable-next-line:jsx-no-lambda
                  onClick={() => this.handleAdd(category.id)}>
                  <AddIcon />
                </Button>
                <Button
                  variant="fab"
                  color="secondary"
                  aria-label="remove"
                  className={this.props.classes.button}
                  mini={true}
                  // tslint:disable-next-line:jsx-no-lambda
                  onClick={() => this.handleSubtract(category.id)}>
                  <RemoveIcon />
                </Button>
              </ListItem>
            )}
          </List>
        </Paper>
      </div>
    );
  }

  private handleAdd(id: number) {
    this.state.categories.map(category => {
      if (category.id === id) {
        category.count++;
      }
    });
    this.forceUpdate();
    // axios.put("/api/v1/record/" + id);
  }

  private handleSubtract(id: number) {
    this.state.categories.map(category => {
      if (category.id === id) {
        category.count--;
      }
    });
    this.forceUpdate();
    // axios.put("/api/v1/record/" + id);
  }

}

export default withStyles(styles)(App);
